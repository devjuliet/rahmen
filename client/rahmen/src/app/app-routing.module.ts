import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamsComponent } from './components/teams/teams.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { SelectedTeamComponent } from './components/teams/selected-team/selected-team.component';
import { SelectedProjectComponent } from './components/projects/selected-project/selected-project.component';
import { SelectedSprintComponent } from './components/projects/selected-project/selected-sprint/selected-sprint.component';
import { RetrospectiveComponent } from './components/projects/selected-project/selected-sprint/retrospective/retrospective.component';
import { MySheetComponent } from './components/projects/selected-project/my-sheet/my-sheet.component';
import { GenSprintComponent } from './components/projects/selected-project/gen-sprint/gen-sprint.component';


const routes: Routes = [
  {path: 'teams', component: TeamsComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'teams/:id', component: SelectedTeamComponent},
  {path: 'projects/:id', component: SelectedProjectComponent},
  {path: 'projects/:id/newSprint', component: GenSprintComponent},
  {path: 'projects/:id/sprint/:sid', component: SelectedSprintComponent},
  {path: 'projects/:id/:sid/retrospective', component: RetrospectiveComponent},
  {path: 'projects/:id/newCard', component: MySheetComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
