import { Component, OnInit } from '@angular/core';
import { CardService } from 'src/app/services/card.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-sheet',
  templateUrl: './my-sheet.component.html',
  styleUrls: ['./my-sheet.component.css']
})
export class MySheetComponent implements OnInit {

  _cardId: Number;
  _sprintId:Number;
  _projectId:String;
  _cardPriority: Number;
  _cardName: String;
  _role: String;
  _feature: String;
  _benefict: String;
  _context: String;
  _status: String;
  _events: [String];
  _results: [String];
  constructor(public cardService:CardService, public router:ActivatedRoute) { }

  ngOnInit() {
    this._projectId =  this.router.snapshot.paramMap.get('id');
  }

  createCard(){
    let obj = {
      _cardId: this._cardId,
      _sprintId:this._sprintId,
      _projectId:this._projectId,
      _cardPriority: this._cardPriority,
      _cardName: this._cardName,
      _role: this._role,
      _feature: this._feature,
      _benefict: this._benefict,
      _context: this._context,
      _status: 'todo',
      _events: this._events,
      _results: this._results
    };

    this.cardService.createCard(obj).subscribe();
  }

}
