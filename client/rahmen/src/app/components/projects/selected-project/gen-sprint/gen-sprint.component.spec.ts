import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenSprintComponent } from './gen-sprint.component';

describe('GenSprintComponent', () => {
  let component: GenSprintComponent;
  let fixture: ComponentFixture<GenSprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenSprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenSprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
