import { Component, OnInit } from '@angular/core';
import { SprintService } from 'src/app/services/sprint.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-gen-sprint',
  templateUrl: './gen-sprint.component.html',
  styleUrls: ['./gen-sprint.component.css']
})
export class GenSprintComponent implements OnInit {

  sprintId : number
  proyectId : any
  constructor(public sprintService:SprintService, public router:ActivatedRoute) { }

  ngOnInit() {
    this.proyectId =  this.router.snapshot.paramMap.get('id');

  }

  insertSprint(){
    let obj ={
      _projectId: this.proyectId,
      _sprintId: this.sprintId,
      _backlogType: "active"
    };
    this.sprintService.createSprint(obj).subscribe();
  }

}
